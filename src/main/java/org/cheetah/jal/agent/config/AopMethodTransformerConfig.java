package org.cheetah.jal.agent.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="method-transformer")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopMethodTransformerConfig {

	
	@XmlAttribute(name="name")
	String name ;
	
	@XmlElement(name="transformer-param")
	@XmlElementWrapper(name="transformer-params")
	List<AopTransformerParam> transformerParams=new ArrayList<AopTransformerParam>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AopTransformerParam> getTransformerParams() {
		return transformerParams;
	}

	public void setTransformerParams(List<AopTransformerParam> transformerParams) {
		this.transformerParams = transformerParams;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopMethodTransformerConfig [name=").append(name).append("]");
		return builder.toString();
	}
	
	
}
