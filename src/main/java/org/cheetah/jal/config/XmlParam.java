package org.cheetah.jal.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="param")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlParam {
	
	@XmlAttribute(name="type")
	String type;
	
	@XmlAttribute(name="order")
	int order;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}
	
	

}
