package org.cheetah.jal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="aop")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlRoot {
	
	@XmlElement(name="logger")
	List<XmlLogger> loggers ;

	public List<XmlLogger> getLoggers() {
		return loggers;
	}

	public void setLoggers(List<XmlLogger> loggers) {
		this.loggers = loggers;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("XmlRoot [loggers=").append(loggers).append("]");
		return builder.toString();
	}

}
