package org.cheetah.jal.agent.transformers.editors;

import javassist.CannotCompileException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

public class TraceAllCallExprEditor extends ExprEditor {

	@Override
	public void edit(MethodCall m) throws CannotCompileException {
		m.replace("{" + "log.debug(\"Calling method " + m.getClassName() + "." + m.getMethodName() + "\");"
				+ "$_ = $proceed($$);" + "}");
	}

}
