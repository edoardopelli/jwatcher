package org.cheetah.jal.agent.transformers;

import java.beans.PropertyDescriptor;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.List;

import org.cheetah.jal.agent.config.AopExecConfig;
import org.cheetah.jal.agent.config.AopMethodTransformerConfig;
import org.cheetah.jal.agent.config.AopParamConfig;
import org.cheetah.jal.agent.config.AopTargetConfig;
import org.cheetah.jal.agent.config.AopTransformerParam;

import javassist.ByteArrayClassPath;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class JAgentTransformer extends AbstractClassFileTransformer {

	@Override
	public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
			ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
		try{
		ClassPool pool = ClassPool.getDefault();
		pool.insertClassPath(new ByteArrayClassPath(className, classfileBuffer));
		List<AopTargetConfig> targets = getTargets();
		className = className.replaceAll("/", ".");

		for (AopTargetConfig target : targets) {
			if (className.equals(target.getClassName())) {
				CtClass ctClass = pool.get(className);
				if (!ctClass.isFrozen()) {
					List<AopExecConfig> execs = target.getExecs();
					for (AopExecConfig exec : execs) {
						String methodName = exec.getMethod();
						List<CtClass> ctParams = new ArrayList<CtClass>();
						for (AopParamConfig p : exec.getParams()) {
							ctParams.add(pool.get(p.getType()));
						}
						CtMethod method = ctClass.getDeclaredMethod(methodName,ctParams.size()==0?null:ctParams.toArray(new CtClass[0]));
						for (AopMethodTransformerConfig methodTransformer : exec.getMethodTransformers()) {
							MethodTransformer m = (MethodTransformer) Class.forName(methodTransformer.getName())
									.newInstance();
							for (AopTransformerParam transformerParam : methodTransformer.getTransformerParams()) {
								String name = transformerParam.getName();
								String value = transformerParam.getValue();
								PropertyDescriptor pd = new PropertyDescriptor(name, m.getClass());
								pd.getWriteMethod().invoke(m, value);
							}
							m.transform(method);
						}

					}
				}
				return ctClass.toBytecode();
			}

		}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

}
