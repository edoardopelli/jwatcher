package org.cheetah.jal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "targetClass")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlTargetClass {

	@XmlAttribute(name = "name")
	String name;
//	@XmlAttribute(name = "type")
//	String type;

	@XmlElement(name = "signature")
	private List<XmlSignature> signatures;

	@XmlElement(name = "call")
	private List<XmlCall> calls;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public List<XmlSignature> getSignatures() {
		return signatures;
	}

	public void setSignatures(List<XmlSignature> signatures) {
		this.signatures = signatures;
	}

	public List<XmlCall> getCalls() {
		return calls;
	}

	public void setCalls(List<XmlCall> calls) {
		this.calls = calls;
	}
}
