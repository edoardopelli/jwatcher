package org.cheetah.jal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "logger")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlLogger {

	@XmlAttribute(name = "level")
	private String level;
	@XmlAttribute(name = "type")
	private String type;

	@XmlElement(name = "targetClass")
	private List<XmlTargetClass> targets;

	public List<XmlTargetClass> getTargets() {
		return targets;
	}

	public void setTargets(List<XmlTargetClass> targets) {
		this.targets = targets;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
