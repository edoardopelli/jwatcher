package org.cheetah.jal.agent.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="transformer-param")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopTransformerParam {

	@XmlAttribute(name="name")
	String name;

	@XmlAttribute(name="value")
	String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopTransformerParam [name=").append(name).append(", value=").append(value).append("]");
		return builder.toString();
	}
}
