package org.cheetah.jal.editors;

import org.cheetah.jal.aop.Watcher;
import org.cheetah.jal.config.XmlCall;
import org.cheetah.jal.config.XmlSignature;

import javassist.CannotCompileException;
import javassist.expr.ExprEditor;
import javassist.expr.FieldAccess;
import javassist.expr.Handler;
import javassist.expr.Instanceof;
import javassist.expr.MethodCall;
import javassist.expr.NewArray;
import javassist.expr.NewExpr;

public class TimeExprEditor extends ExprEditor {

	
	private XmlCall call;
	
	private Watcher watcher;
	
	

	public TimeExprEditor(XmlCall call, Watcher watcher) {
		super();
		this.call = call;
		this.watcher = watcher;
	}



	@Override
	public void edit(MethodCall m) throws CannotCompileException {
		try {
			Class objectImplemented = Class.forName(call.getClassName());
			Class objectInvoked = Class.forName( m.getClassName());//classe su cui viene fatta l'invocazione
			if(m.getClassName().equals(call.getClassName()) || objectInvoked.isAssignableFrom(objectImplemented)) {
				for (XmlSignature _signature : call.getSignatures()) {
					if( m.getMethodName().equals(_signature.getName())){
						watcher.onCall(m);
						//se trovo il metodo interrompo il ciclo, in quanto la chiamata � stata sostituita dal codice iniettato
						//inoltre andare avanti su altri nomi di metodi porterebbe un errore sull'oggetto di tipo MethodCall 
						break;
						
					}
				}
			}
//			System.out.println(m.getClassName()+" -- "+m.getMethodName()+"-->"+objectInvoked.isAssignableFrom(objectImplemented));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	@Override
	public void edit(FieldAccess f) throws CannotCompileException {
//		System.out.println("TimeExprEditor.edit()::FieldAccess");
//		System.out.println(f.getClassName()+"."+f.getFieldName());
		super.edit(f);
	}












	
	
}
