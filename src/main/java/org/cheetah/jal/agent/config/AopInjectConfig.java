package org.cheetah.jal.agent.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="aop")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopInjectConfig {
	
	@XmlElement(name="transformer")
	List<AopTransformerConfig> transformers = new ArrayList<AopTransformerConfig>() ;

	public List<AopTransformerConfig> getTranformers() {
		return transformers;
	}

	public void setTranformers(List<AopTransformerConfig> targets) {
		this.transformers = targets;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopInjectConfig [transformers=").append(transformers).append("]");
		return builder.toString();
	}

}
