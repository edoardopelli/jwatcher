package org.cheetah.jal.agent;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.Instrumentation;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.cheetah.jal.agent.config.AopInjectConfig;
import org.cheetah.jal.agent.config.AopTransformerConfig;
import org.cheetah.jal.agent.transformers.AbstractClassFileTransformer;
import org.xml.sax.InputSource;

public class JWatcherAgent {
	
	
	
	public static void premain(String agentArgs, Instrumentation inst) throws Exception{
		JAXBContext jc = JAXBContext.newInstance(AopInjectConfig.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		AopInjectConfig root = (AopInjectConfig) unmarshaller.unmarshal(new InputSource(JWatcherAgent.class.getResourceAsStream("/inject-aop.xml")));
		
		for (AopTransformerConfig tranformer : root.getTranformers()) {
			ClassFileTransformer transformer = (ClassFileTransformer) Class.forName(tranformer.getName()).newInstance();
			if(!(transformer instanceof AbstractClassFileTransformer)){
				throw new RuntimeException("Transformer is not an instance of "+AbstractClassFileTransformer.class);
			}
			((AbstractClassFileTransformer)transformer).init(tranformer);
			inst.addTransformer(transformer);
		}
		

	}

	
	
}
