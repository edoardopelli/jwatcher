package org.cheetah.jal.aop;

import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.expr.MethodCall;

public class TimeWatcher implements Watcher, LogWatcher {

	private static final String DEFAULT_LEVEL="INFO";
	
	private String level = DEFAULT_LEVEL ;
	
	/**
	 * 
	 * @param level
	 */
	public void initLevel(String level){
		this.level = level;
	}
	
	@Override
	public void onExec(CtMethod ctMethod) throws CannotCompileException {
		ctMethod.insertAt(0, "log."+getMethod()+"(\""+ctMethod.getDeclaringClass().getName()+"::"+ctMethod.getName()+" - BEGIN\");");
		ctMethod.insertAt(1,"{"
				+ "for(int i=0; i<$args.length;i++){"
				+ "log."+getMethod()+"(\"\\t\\tParam[\"+i+\"]:\"+$args[i]);"
				+ "}"
				+ "}");
		ctMethod.addLocalVariable("start", CtClass.longType);
		ctMethod.addLocalVariable("end", CtClass.longType);
		ctMethod.addLocalVariable("totale", CtClass.longType);
		ctMethod.insertBefore("start = System.currentTimeMillis();");
		ctMethod.insertAfter("end = System.currentTimeMillis();");
		ctMethod.insertAfter("totale = end - start;");
		ctMethod.insertAfter("log."+getMethod()+"(\"Execution time: \"+totale+\"\");");
		ctMethod.insertAfter("log."+getMethod()+"(\""+ctMethod.getDeclaringClass().getName()+"::"+ctMethod.getName()+" - END\");",true);
				
	}

	private String getMethod() {
		String method = this.level.toLowerCase();
		return method;
	}
	@Override
	public void onCall(MethodCall m) throws CannotCompileException {
		m.replace("{long start = System.currentTimeMillis();"
				+ "$_ = $proceed($$);"
				+ "long end = System.currentTimeMillis();"
				+ "long tot = end - start;"
				+ "log."+getMethod()+"(\"Execution time "+m.getClassName()+"::"+m.getMethodName()+" - \"+tot+\"ms\");"
				+ "}");		
	}


	

//	private CtMethod extractMethodByPArams(ClassPool pool, CtClass ctClass, XmlSignatureConfig signature,
//			String methodName) throws NotFoundException {
//		List<CtClass> params = getParams	(pool, signature);
//
//		CtMethod ctMethod = ctClass.getDeclaredMethod(methodName, params.toArray(new CtClass[0]));
//		return ctMethod;
//	}
//
//	private List<CtClass> getParams(ClassPool pool, XmlSignatureConfig signature) throws NotFoundException {
//		List<CtClass> params = new ArrayList<CtClass>();
//		for (XmlParamConfig p : signature.getParams()) {
//			params.add(pool.get(p.getType()));
//		}
//		return params;
//	}
}
