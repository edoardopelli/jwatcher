package org.cheetah.jal.agent.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="transformer")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopTransformerConfig {

	
	@XmlAttribute(name="name")
	String name;
	
	@XmlElement(name="target")
	List<AopTargetConfig> targets = new ArrayList<AopTargetConfig>();

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopTransformerConfig [name=").append(name).append(", target=").append(targets).append("]");
		return builder.toString();
	}

	public List<AopTargetConfig> getTargets() {
		return targets;
	}

	public void setTargets(List<AopTargetConfig> targets) {
		this.targets = targets;
	}


	
	
}
