package org.cheetah.jal.agent.transformers;

import javassist.CtMethod;

public interface MethodTransformer {

	
	public void transform(CtMethod method) throws Exception;
}
