package org.cheetah.jal.agent.config;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="param")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopParamConfig {

	@XmlAttribute(name="type")
	String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopParamConfig [type=").append(type).append("]");
		return builder.toString();
	}
	
	
	
}
