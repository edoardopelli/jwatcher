package org.cheetah.jal;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cheetah.jal.aop.LogWatcher;
import org.cheetah.jal.aop.Watcher;
import org.cheetah.jal.config.XmlCall;
import org.cheetah.jal.config.XmlLogger;
import org.cheetah.jal.config.XmlParam;
import org.cheetah.jal.config.XmlSignature;
import org.cheetah.jal.config.XmlTargetClass;
import org.cheetah.jal.editors.TimeExprEditor;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

public final class AopWriter {

	private static AopWriter writer;
	
	private AopWriter(){}
	
	public static AopWriter getInstance() {
		
		return writer==null? new AopWriter() : writer; 
	}

	public void writeCode(XmlLogger loggerConfig) throws Exception {
		Watcher watcher = (Watcher) Class.forName(loggerConfig.getType()).newInstance();
//		logCreate.createAop(loggerConfig);
		
		
		ClassPool pool = ClassPool.getDefault();
		if(watcher instanceof LogWatcher){
			((LogWatcher)watcher).initLevel(loggerConfig.getLevel());
		}
		
		for(XmlTargetClass target : loggerConfig.getTargets()){
			CtClass ctClass = pool.get(target.getName());
			CtClass ctLogger = pool.get(Logger.class.getName());
			CtField ctFieldLogger = CtField.make("private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("+target.getName()+".class) ;", ctClass);
			ctClass.addField(ctFieldLogger);
			for(XmlSignature signature : target.getSignatures()){
				List<XmlParam> params = signature.getParams();
				CtClass[] ctParams = createParams(params);
				CtMethod ctMethod = ctClass.getDeclaredMethod(signature.getName(), ctParams);
				
				watcher.onExec(ctMethod);
				
				for (final XmlCall call : signature.getCalls()) {
					ctMethod.instrument(new TimeExprEditor(call,watcher));
				}
			}
			ctClass.toClass();
		}
		
		new TestClass().test("ewewWS", 2);
	
		
	}
	
	private CtClass[] createParams(List<XmlParam> params) throws NotFoundException {
		List<CtClass> temp = new ArrayList<CtClass>();
		for (XmlParam xmlParam : params) {
			temp.add(ClassPool.getDefault().get(xmlParam.getType()));
		}
		return temp.toArray(new CtClass[0]);
	}

}
