package org.cheetah.jal.agent.transformers;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;
import java.util.List;

import org.cheetah.jal.agent.config.AopTargetConfig;
import org.cheetah.jal.agent.config.AopTransformerConfig;

public abstract class AbstractClassFileTransformer implements ClassFileTransformer {
	
	private AopTransformerConfig config;

	public final void init(AopTransformerConfig config){
		this.config=config;
	}

	public final List<AopTargetConfig> getTargets() {
		// TODO Auto-generated method stub
		return this.config.getTargets();
	}


}
