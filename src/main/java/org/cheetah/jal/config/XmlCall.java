package org.cheetah.jal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="call")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlCall {

	@XmlAttribute(name="class")
	private String className;
	
	@XmlElement(name="signature")
	private List<XmlSignature> signatures;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<XmlSignature> getSignatures() {
		return signatures;
	}

	public void setSignatures(List<XmlSignature> signatures) {
		this.signatures = signatures;
	}
}
