package org.cheetah.jal.agent.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="exec")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopExecConfig {

	@XmlAttribute(name="method")
	String method;
	
	@XmlElement(name="param")
	@XmlElementWrapper(name="params")
	List<AopParamConfig> params = new ArrayList<AopParamConfig>();
	
	@XmlElement(name="method-transformer")
	List<AopMethodTransformerConfig> methodTransformers = new ArrayList<AopMethodTransformerConfig>();


	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public List<AopParamConfig> getParams() {
		return params;
	}

	public void setParams(List<AopParamConfig> params) {
		this.params = params;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopExecConfig [method=").append(method).append(", params=").append(params).append("]");
		return builder.toString();
	}

	public List<AopMethodTransformerConfig> getMethodTransformers() {
		return methodTransformers;
	}

	public void setMethodTransformers(List<AopMethodTransformerConfig> methodTransformers) {
		this.methodTransformers = methodTransformers;
	}

	
}
