package org.cheetah.jal.aop;

import org.cheetah.jal.config.XmlLogger;

import javassist.CannotCompileException;
import javassist.CtMethod;
import javassist.expr.MethodCall;

public interface Watcher {

	public void onExec(CtMethod ctMethod) throws CannotCompileException;
	
	public void onCall(MethodCall m) throws CannotCompileException;
}
