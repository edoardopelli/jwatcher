package org.cheetah.jal.agent.transformers;

import org.apache.log4j.lf5.LogLevel;

import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;

public class TimeWatcherMethodTransformer implements MethodTransformer {
	
	private String level;

	@Override
	public void transform(CtMethod ctMethod) throws Exception {
		
		CtClass ownerClass = ctMethod.getDeclaringClass();
		CtField ctFieldLogger = CtField.make("private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("+ownerClass.getName()+".class) ;", ownerClass);
		ownerClass.addField(ctFieldLogger);
		
		ctMethod.insertAt(0, "log."+getLevel()+"(\""+ctMethod.getDeclaringClass().getName()+"::"+ctMethod.getName()+" - BEGIN\");");
		ctMethod.insertAt(1,"{"
				+ "for(int i=0; i<$args.length;i++){"
				+ "log."+getLevel()+"(\"\\t\\tParam[\"+i+\"]:\"+$args[i]);"
				+ "}"
				+ "}");
		ctMethod.addLocalVariable("start", CtClass.longType);
		ctMethod.addLocalVariable("end", CtClass.longType);
		ctMethod.addLocalVariable("totale", CtClass.longType);
		ctMethod.insertBefore("start = System.currentTimeMillis();");
		ctMethod.insertAfter("end = System.currentTimeMillis();");
		ctMethod.insertAfter("totale = end - start;");
		ctMethod.insertAfter("log."+getLevel()+"(\"Execution time: \"+totale+\"\");");
		ctMethod.insertAfter("log."+getLevel()+"(\""+ctMethod.getDeclaringClass().getName()+"::"+ctMethod.getName()+" - END\");",true);
	}

	public String getLevel() {
		return level==null?null:level.toLowerCase();
	}

	public void setLevel(String level) {
		this.level = level;
	}

}
