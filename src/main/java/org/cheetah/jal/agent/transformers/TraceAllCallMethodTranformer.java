package org.cheetah.jal.agent.transformers;

import org.cheetah.jal.agent.transformers.editors.TraceAllCallExprEditor;

import javassist.CtMethod;

public class TraceAllCallMethodTranformer implements MethodTransformer {

	@Override
	public void transform(CtMethod method) throws Exception {

		method.instrument(new TraceAllCallExprEditor());
		
	}

}
