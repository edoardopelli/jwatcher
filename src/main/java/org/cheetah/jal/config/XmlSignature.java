package org.cheetah.jal.config;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="signature")
@XmlAccessorType(XmlAccessType.FIELD)
public class XmlSignature {

	@XmlAttribute(name="name")
	private String name;
	
	@XmlElement(name="param")
	@XmlElementWrapper(name="params")
	private List<XmlParam> params;
	
	@XmlElement(name = "call")
	private List<XmlCall> calls;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<XmlParam> getParams() {
		return params;
	}

	public void setParams(List<XmlParam> params) {
		this.params = params;
	}

	public List<XmlCall> getCalls() {
		return calls;
	}

	public void setCalls(List<XmlCall> calls) {
		this.calls = calls;
	}
}
