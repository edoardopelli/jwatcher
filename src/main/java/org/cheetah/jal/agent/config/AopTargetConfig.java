package org.cheetah.jal.agent.config;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="transformers")
@XmlAccessorType(XmlAccessType.FIELD)
public class AopTargetConfig {

	@XmlAttribute(name="class")
	String className;
	
	@XmlElement(name="exec")
	List<AopExecConfig> execs = new ArrayList<AopExecConfig>();

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public List<AopExecConfig> getExecs() {
		return execs;
	}

	public void setExecs(List<AopExecConfig> execs) {
		this.execs = execs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AopTargetConfig [className=").append(className).append(", execs=").append(execs).append("]");
		return builder.toString();
	}
}
